import { Injectable } from '@angular/core';
import { Observable,of } from "rxjs";
import { StudentService } from '../service/student-service';
import { Student } from "../entity/student";
@Injectable({
  providedIn: 'root'
})
export class StudentDataImplServiceEX extends StudentService {
	
	getStudents(): Observable<Student[]> {
		return of(this.students);
	};
	genRandomGpa() : number {
		return Math.round((Math.random() * 400))/100;
	}
	students: Student[] = [{
    id: 3,
    studentId: "123456789",
    name: 'John',
    surname: 'Cena',
    gpa: this.genRandomGpa()
  }, {
    id: 4,
    studentId: "987654321",
    name: 'Random',
    surname: 'Guy2',
    gpa: this.genRandomGpa()
  },
   {
    id: 5,
    studentId: "602115023",
    name: 'Somruk',
    surname: 'Laothamjinda',
    gpa: this.genRandomGpa()
  }];

  	
}
