import { TestBed, inject } from '@angular/core/testing';

import { StudentDataImplExService } from './student-data-impl-ex.service';

describe('StudentDataImplExService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentDataImplExService]
    });
  });

  it('should be created', inject([StudentDataImplExService], (service: StudentDataImplExService) => {
    expect(service).toBeTruthy();
  }));
});
